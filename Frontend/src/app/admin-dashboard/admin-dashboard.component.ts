import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LeadService } from '../lead/lead.service';
import { AdminDashboardService } from './admin-dashboard.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  LeadsData: any = [];
  OfferingData: any = [];
  config: any;
  config1: any;
  UserInvestorData: any;
  HeaderSummaryData: any;
  VerifyAccountPopup: boolean = false;
  VerifyPopupShow: boolean = false;
  Loader: boolean = false;
  VerifyAccountId: any;
  UserId: any;

  constructor(private adminDashboardService: AdminDashboardService,
    private leadService : LeadService,
    private router: Router) {
    this.config = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: this.LeadsData.length
    };
    this.config1 = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: this.OfferingData.length
    };
  }

  ngOnInit(): void {
    this.UserId = Number(localStorage.getItem("UserId"));
    this.Loader = true;
    this.GetLeads();
    this.GetOffering();
    this.GetUserInvestorDetails();
    this.GetAdminHeaderSummary();
  }

  GetLeads() {
    this.adminDashboardService.GetLeads().subscribe(data => {
      this.LeadsData = data;
      console.log(this.LeadsData, 'leads')
      this.Loader = false;
    })
  }

  GetOffering() {
    this.adminDashboardService.GetOffering().subscribe(data => {
      this.OfferingData = data;
      console.log(this.OfferingData, 'this.OfferingData')
    })
  }

  onVerifyAccount(val: any) {
    this.VerifyAccountPopup = true;
    if(val.verifyAccount == true){
      this.VerifyPopupShow = true;
    }
    else{
      this.VerifyPopupShow = false;
    }
    this.VerifyAccountId = val.id;
    console.log(val,'verify')
  }

  pageChanged(event: any) {
    this.config.currentPage = event;
  }

  pageChanged1(event: any) {
    this.config1.currentPage = event;
  }

  GetUserInvestorDetails() {
    this.adminDashboardService.GetUserInvestorDetails().subscribe(data => {
      this.UserInvestorData = data;
      console.log(this.UserInvestorData, 'UserInvestorData')
    })
  }

  GetAdminHeaderSummary() {
    this.adminDashboardService.GetAdminHeaderSummary().subscribe(data => {
      this.HeaderSummaryData = data;
      console.log(this.HeaderSummaryData, 'this.HeaderSummaryData ')
    })
  }

  VerifyUser(){
    this.Loader = true;
    this.leadService.VerifyAccount(this.UserId, this.VerifyAccountId, this.VerifyPopupShow).subscribe(data => {
      if (data == true) {
        this.VerifyAccountPopup = false;
        this.GetLeads();
      }
      else {
        this.VerifyAccountPopup = false;
        this.Loader = false;
      }
    })
  }

  VerifyCancel(){
    this.VerifyAccountPopup = false;
    let x = this.LeadsData.filter((x: { id: any; }) => x.id == this.VerifyAccountId);
    if(x.length > 0){
      x[0].verifyAccount = !x[0].verifyAccount
    }
  }
}
